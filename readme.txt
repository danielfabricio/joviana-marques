<a href="#something">Show</a>
<a href="#close">Close</a>

<div id="close"> Close </div>
<div id="something"> Bingo! </div>


CSSS >

#something{
  display: none;
}

#something:target {
  display: block;
}

#close:target {
  display: none;
}

#close:target + #something{
  display: none;
}
