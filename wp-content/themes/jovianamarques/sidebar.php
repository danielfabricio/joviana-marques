<!-- sidebar -->
<aside class="sidebar" role="complementary">

	<?php get_template_part('searchform'); ?>

	<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-blog')) ?>	

</aside>
<!-- /sidebar -->
