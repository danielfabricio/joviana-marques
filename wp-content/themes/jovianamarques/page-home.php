<?php /* Template Name: Home */ ?>

<?php get_header(); ?>

	<?php query_posts($destaque_query); while (have_posts()) : the_post(); ?>
		
		<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
			<!-- CONTEÚDO PRINCIPAL -->
			<main class="main">
				<div class="bg-home" style="background-image:url(<?php the_post_thumbnail_url(); ?>);"></div>
			</main>
			<!-- //CONTEÚDO PRINCIPAL -->
		<?php endif; ?>
			
	<?php endwhile; ?>


<?php get_footer(); ?>
