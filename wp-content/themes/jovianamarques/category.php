<?php get_header(); ?>

	<div class="container-full-height">
		<!-- CONTEÚDO PRINCIPAL -->
		<main class="main container-full-width">

			<!-- MENU GALERIA -->
			<nav class="galeria-nav">
				<ul>
					<li><a href="#" class="btn txt-pre big grey tip active">Personal Work</a></li>
					<li><a href="#" class="btn txt-pre big grey tip">Work</a></li>
					<li><a href="#" class="btn txt-pre big grey tip">Sketch</a></li>
				</ul>
			</nav>
			<!-- //MENU GALERIA -->
			
			<!-- LISTAGEM DA GALERIA -->
			<div class="galeria-content">
				<div class="galeria-itens">


				<?php

					if( get_query_var( 'paged' ) ){
						$paged = get_query_var('paged');
					}elseif( get_query_var('page') ){
						$paged = get_query_var('page');
					}else{
						$paged = 1;
					}

					$cat = get_query_var('cat');
					$args  = array(
						'post_type'=>'galeria',
						'posts_per_page'=> wp_is_mobile() ? '6' : '9',
						'paged'=>$paged,
						'cat'=>$cat			
					);
					
				?>
					
				<?php query_posts($args); while (have_posts()) : the_post(); ?>

					<div class="galeria-item">
						<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail('galeria-thumb'); ?>
							</a>
						<?php endif; ?>
					</div>

				<?php endwhile; ?>

				</div>

				<?php get_template_part('pagination'); ?>

			</div>
			<!-- //LISTAGEM DA GALERIA -->

		</main>
		<!-- //CONTEÚDO PRINCIPAL -->
	</div>

<?php get_footer(); ?>
