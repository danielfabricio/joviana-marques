<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=301560246616198&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<style>
	.fb_iframe_widget_fluid span{width: 100% !important;}
</style>

<!-- COMENTÁRIOS -->
<div class="comments block">
	<div class="comments-title block-title">
		<h4>Comentários</h4>
	</div>
	<div class="comments-content">
		<?php $url = get_permalink( $post->ID ); ?>
		<div class="fb-comments" data-href="<?php echo $url;?>" data-width="100%" data-numposts="5" data-colorscheme="light"></div>
	</div>
</div>
<!-- /COMENTÁRIOS -->