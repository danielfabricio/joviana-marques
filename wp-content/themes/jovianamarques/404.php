<?php get_header(); ?>

	<div class="container-full-height">
		<div class="container">
			<article id="post-404">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				
				<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>

			</article>
			<!-- /article -->

		</div>
	</div>

<?php get_footer(); ?>
