<?php /* Template Name: Página Padrão */ ?>

<?php get_header(); ?>

	<div class="container-full-height">
		<!-- CONTEÚDO PRINCIPAL -->
		<main class="main half-half" role="main">
			<!-- TELA DIVIDIDA -->

			<?php if (have_posts()): while (have_posts()) : the_post(); ?>

				
			
				<div class="half-screen half-1" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
					&nbsp;
				</div>

				<div class="half-screen half-2">

					<header class="page-top">
						<h1><?php the_title(); ?></h1>
						<div class="page-share">Share links</div>
					</header>
						
					<article class="article">

						<div class="page-content">
							<?php the_content(); ?>

							
							<?php if( get_field('link_para_galeria') ): ?>
								<div class="content-box galeria-link" style="background-image: url(<?php the_field('backgroud_do_link'); ?>);">
									<a href="<?php the_field('link_para_galeria'); ?>" class="btn bigger ghost white">Ver galeria</a>
								</div>
							<?php endif; ?>

							<br/>
							<?php edit_post_link(); ?>
						</div>

						

					</article>

				</div>

			<?php endwhile; ?>

			<?php else: ?>
				
				<h2><?php _e( 'Desculpa, esta página está vazia.', 'html5blank' ); ?></h2>

			<?php endif; ?>

			<!-- //TELA DIVIDIDA -->
		</main>
		<!-- //CONTEÚDO PRINCIPAL -->		
	</div>


<?php get_footer(); ?>
