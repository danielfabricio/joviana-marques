<?php /* Template Name: Galerias - Work */ ?>

<?php get_header(); ?>

	<div class="container-full-height">
		<!-- CONTEÚDO PRINCIPAL -->
		<main class="main container-full-width">

			<!-- MENU GALERIA -->
			<?php navgaleria(); ?>
			<!-- //MENU GALERIA -->

			
			<!-- LISTAGEM DA GALERIA -->
			<div class="galeria-content">
				<div class="galeria-itens">


				<?php

					if( get_query_var( 'paged' ) ){
						$paged = get_query_var('paged');
					}elseif( get_query_var('page') ){
						$paged = get_query_var('page');
					}else{
						$paged = 1;
					}

					$args  = array(
						'post_type'=>'galerias',
						'posts_per_page'=> wp_is_mobile() ? '6' : '9',
						'paged'=>$paged,
						'category_name'=>'work'			
					);
					
				?>
					
				<?php query_posts($args); while (have_posts()) : the_post(); ?>

					<?php include "galeria-loop.php"; ?>

				<?php endwhile; ?>

				</div>

				<?php get_template_part('pagination'); ?>

			</div>
			<!-- //LISTAGEM DA GALERIA -->

		</main>
		<!-- //CONTEÚDO PRINCIPAL -->
	</div>

<?php get_footer(); ?>
