<div class="share-links">
	<!-- Place this tag where you want the su badge to render -->
	<su:badge layout="1"></su:badge>
	<a href="https://twitter.com/share" class="twitter-share-button" data-via="jovianamarques">Tweet</a>
	<div class="fb-like" data-href="<?php echo $url;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>


	<!-- Place this snippet wherever appropriate -->
	<script type="text/javascript">
	(function() {
	var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
	li.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//platform.stumbleupon.com/1/widgets.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
	})();
	</script>

	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=301560246616198&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
</div>