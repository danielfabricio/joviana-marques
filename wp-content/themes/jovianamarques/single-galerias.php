<?php get_header(); ?>
<div class="container-full-height">

	<!-- MENU GALERIA -->
	<nav class="galeria-nav">
		<ul>
			<li><a href="#" class="btn txt-pre big grey tip active">Personal Work</a></li>
			<li><a href="#" class="btn txt-pre big grey tip">Work</a></li>
			<li><a href="#" class="btn txt-pre big grey tip">Sketch</a></li>
		</ul>
	</nav>
	<!-- //MENU GALERIA -->

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
		<!-- CONTEÚDO PRINCIPAL -->
		<main class="main galery-main">
			<?php if ( has_post_thumbnail()) :?>
				<div class="galery-main-bg" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
			<?php endif; ?>
			
			<div class="container">
				<!-- SIDE-IMAGE -->
				<div class="side-image">
					<?php if ( has_post_thumbnail()) :?>
						<?php the_post_thumbnail('side-thumb');?>
					<?php endif; ?>
				</div>
				<!-- //SIDE-IMAGE -->

				<!-- CONTENT TEXT -->
				<article class="galery-content">
					<h1><?php the_title(); ?></h1>

					<div class="content">
						<?php the_content(); ?>
					</div>
					
					<?php if( get_field('link_para_compra') ): ?>
						<div class="shop-link">
							<a href="<?php the_field('link_para_compra'); ?>" class="btn dark-grey small txt-pre">Print avaliable</a>
						</div>
					<?php endif; ?>
					
				</article>
				<!-- //CONTENT TEXT -->
			</div>
		</main>
		<!-- //CONTEÚDO PRINCIPAL -->
	<?php endwhile; ?>

	<?php else: ?>
		<!-- CONTEÚDO PRINCIPAL -->
		<main class="main galery-main">
			<div class="galery-main-bg"></div>
			<div class="container">
				<!-- SIDE-IMAGE -->
				<div class="side-image"></div>
				<!-- //SIDE-IMAGE -->

				<!-- CONTENT TEXT -->
				<div class="galery-content">
					<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
				</div>
				<!-- //CONTENT TEXT -->
			</div>
		</main>
		<!-- //CONTEÚDO PRINCIPAL -->
	<?php endif; ?>

	
	<!-- MORE - LISTAGEM DA GALERIA -->
	<div class="galery-more container-full-width">
		<div class="galeria-content">
			<div class="galeria-itens">
				<?php wp_reset_query(); ?>

				<?php 
					$this_post = $post->ID;

					$args = array(
						'category__in' => wp_get_post_categories($post->ID),
						'posts_per_page'=> wp_is_mobile() ? '6' : '3',
						'post__not_in' => array($this_post),
						'post_type'=>'galerias'
					);

					$related = new WP_Query( $args );
				?>
					
				<?php if ( $related->have_posts() ) : while ( $related->have_posts() ) : $related->the_post(); ?>

					<div class="galeria-item">
						<?php if ( has_post_thumbnail()) : ?>
							<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<?php the_post_thumbnail('galeria-thumb'); ?>
							</a>
						<?php endif; ?>
					</div>

				<?php endwhile; ?>
				<?php endif; ?>

				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
	<!-- // MORE - LISTAGEM DA GALERIA -->


</div>

<?php get_footer(); ?>
