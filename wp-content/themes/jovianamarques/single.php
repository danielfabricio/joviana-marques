<?php get_header(); ?>

	<div class="container-full-height">
		<div class="container">

		<main class="main">
			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
				<!-- article -->
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="item-top">
						<h1 class="item-title"><?php the_title(); ?></h1>
					</div>
					<div class="item-content">
						<div class="item-thumb">
							<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<?php the_post_thumbnail('thumb-blog');?>
							<?php endif; ?>
						</div>
						
						<!-- CONTEÚDO -->
						<div class="post-content">
							<?php the_content(); // Dynamic Content ?>
						</div>

						<?php edit_post_link();?>
					</div>
					<div class="item-footer">

						<?php if(has_tag()): ?>
						<div class="tags icon-tags">
							<?php the_tags( __( '', 'html5blank' ), ' '); // Separated by commas with a line break at the end ?>
						</div>
						<?php endif ?>

						<time class="post-data icon-calendario" datetime="<?php the_time('Y-m-j'); ?>"><?php the_time('F j, Y'); ?></time>
						
					</div>

					<?php comments_template(); ?>

				</article>
				<!-- /article -->

			<?php endwhile; ?>

			<?php else: ?>

				<!-- article -->
				<article>

					<h1><?php _e( 'Ops, nada a ser exibido nessa página.', 'html5blank' ); ?></h1>

				</article>
				<!-- /article -->
			<?php endif; ?>
		</main>

		<?php get_sidebar(); ?>

		</div>
	</div>

<?php get_footer(); ?>
