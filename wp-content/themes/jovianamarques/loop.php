<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class('post-item'); ?>>
		<div class="item-top">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<h1 class="item-title"><?php the_title(); ?></h1>
			</a>
			<div class="share-links">Share link</div>
		</div>
		<div class="item-content">
			<div class="item-thumb">
				<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<?php the_post_thumbnail('thumb-blog'); // Declare pixel size you need inside the array ?>
					</a>
				<?php endif; ?>
			</div>
			<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
		</div>
		<div class="item-footer">

			<?php if(has_tag()): ?>
			<div class="tags icon-tags">
				<?php the_tags( __( '', 'html5blank' ), ' '); // Separated by commas with a line break at the end ?>
			</div>
			<?php endif ?>

			<time class="post-data icon-calendario" datetime="<?php the_time('Y-m-j'); ?>"><?php the_time('F j, Y'); ?></time>
			
		</div>
	</article>

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<article>
		<h2><?php _e( 'Desculpa, nenhum post encontrado', 'html5blank' ); ?></h2>
	</article>
	<!-- /article -->

<?php endif; ?>
