		<!-- footer -->
		<footer class="footer" role="contentinfo">

			<!-- copyright -->
			<p class="copyright">
				&copy; <?php echo date('Y'); ?> Copyright <?php bloginfo('name'); ?>. 2016 - Conteúdo protegido por Creative Commons.    Developed by: <a href="http://danielfabricio.com.br">Daniel Fabricio</a>
			</p>
			<!-- /copyright -->

		</footer>
		<!-- /footer -->

		<?php wp_footer(); ?>

	</body>
</html>
