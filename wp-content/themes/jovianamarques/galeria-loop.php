<div class="galeria-item">
	<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php the_post_thumbnail('galeria-thumb'); ?>
		</a>
	<?php endif; ?>
</div>