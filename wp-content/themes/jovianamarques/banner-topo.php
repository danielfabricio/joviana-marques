<?php wp_reset_query(); ?>
<?php $banner_query = array( 
	'post_type' => 'banners',
	'posts_per_page' => '1',
	'orderby' => 'rand'
); ?>

<?php query_posts($banner_query); while (have_posts()) : the_post(); ?>

	<?php if(is_mobile() && get_field('banner_mobile') ): ?>

		<div class="banner-full">

			<?php $image = get_field('banner_mobile');  ?>
			<?php if( !empty($image) ): ?>

				<?php 
					$size = 'banner-mobile';
					$thumb = $image['sizes'][ $size ];
				?>
				<a href="<?php the_field('link_do_banner'); ?>">
					<img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

			<?php endif; ?>

		</div>

	<?php elseif ( get_field('banner_desktop') ):?>

		<div class="banner-full">

			<?php $image = get_field('banner_desktop');  ?>
			<?php if( !empty($image) ): ?>

				<?php 
					$size = 'banner-desktop';
					$thumb = $image['sizes'][ $size ];
				?>

				<a href="<?php the_field('link_do_banner'); ?>">
					<img src="<?php echo $thumb; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>

			<?php endif; ?>

		</div>

	<?php endif; ?>

<?php endwhile; ?>

<?php wp_reset_query(); ?>