<?php /* Template Name: Blog */ ?>

<?php get_header(); ?>

	<div class="container-full-height">
		<div class="container">
			
			<!-- BANNER TOP -->
			<?php include "banner-topo.php"; ?>
			<!-- //BANNER TOP -->
			
			<!-- MAIN - LISTAGEM DE ARTICLES -->
			<main class="main">
				<div class="post-list">

					<?php
						wp_reset_query();

						if( get_query_var( 'paged' ) ){
							$paged = get_query_var('paged');
						}elseif( get_query_var('page') ){
							$paged = get_query_var('page');
						}else{
							$paged = 1;
						}

						$args  = array(
							'post_type'=>'post',
							'posts_per_page'=> wp_is_mobile() ? '6' : '10',
							'paged'=>$paged					
						);
						
						query_posts($args);
						get_template_part('loop'); 
					?>

				</div>

				<?php get_template_part('pagination'); ?>

			</main>
			<!-- //MAIN - LISTAGEM DE ARTICLES -->

			<!-- SIDEBAR -->
			<?php get_sidebar('sidebar-blog'); ?>
			<!-- //SIDEBAR -->

		</div>	
	</div>

<?php get_footer(); ?>
