<!-- search -->
<div class="search block">
	<form class="search" method="get" action="<?php echo home_url(); ?>" role="search">
		<input class="search-input" type="search" name="s" placeholder="Buscar">
		<button class="search-submit icon-search btn dark-grey" type="submit" role="button"></button>
	</form>
</div>
<!-- /search -->
