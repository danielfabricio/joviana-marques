<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'joviana');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pAcCTKF&ouluL*;|3OBEMb=6bnG-7mZFw101QEq<].A[/BInpO+xj{PZ,mGKmQR1');
define('SECURE_AUTH_KEY',  '{-/*mS/;L4>V;n%K?,M]{e-O#F=<B><e1+Q[=&f${;69-B_/?q||#_4U<a+@`PFf');
define('LOGGED_IN_KEY',    'iv|!<)*>j@KR?DJrr#L%s`<GN|_D_fW#d#WTq_&1pEGb40U7%+eW@2VI WQrOwWR');
define('NONCE_KEY',        'hLazzBHPx5OECbHOch^3@|hY^:y*JM|<Ly:wS:yp]S%M2ntFALjv$[?y:&[on6}-');
define('AUTH_SALT',        '%_g,P%[6UzL@DLPfLWp7$1)tJC9y}G,|Z5{:x*p.25[uDA/YkR2Mc1%zexd1-:r+');
define('SECURE_AUTH_SALT', '!#_-KXrJ*NNg}W,;Jyk?QnGgz6)}u()dJ1.ox/bs[Y)ta5<tQ5p{XWyAM=zR<@iO');
define('LOGGED_IN_SALT',   '++/in?I*`pT9;k;q3ff(3F3uCQ^(kHGi0&V5.[#m_,5#6*HM-R*e!+e0tD&dq1]`');
define('NONCE_SALT',       'i[URC+95(A+8W+dSLC$%]QIEs^X;:u*x{DP[7v`k)1Y-X|Y{^5t&{ H[~r[;DAiz');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
