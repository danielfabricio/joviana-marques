module.exports = function(grunt) {

    var packageFile = grunt.file.readJSON('package.json');

    /*
     * ------------------------------------------------------
     * Gera a string com os autores do projeto
     * ------------------------------------------------------
     */
    var authors     = "";

    for( i in packageFile.authors ){
        if( authors == "" ){
            authors = packageFile.authors[i];
        }else{
            authors = authors+"\n\t  "+packageFile.authors[i];
        }
    }
    /*
     * ------------------------------------------------------
     */

    /*
     * ------------------------------------------------------
     * Banners dos arquivos
     * ------------------------------------------------------
     */
    var hr = '----------------------------------------------------\n';
    var bannerFiles = '/*\n' +
        'Theme Name: Joviana Marques\n'+
        'Theme URI: jovianamarques.com.br\n'+
        'Description: 2elve desenvolvimento web\n'+
        'Version: 1.0\n'+
        'Author: Daniel Fabricio\n'+
        'License: MIT\n'+
        'License URI: http://opensource.org/licenses/mit-license.php\n'+
        '*/\n';
    /*
     * ------------------------------------------------------
     */

    /*
     * Configurações das Tasks 
     */
    grunt.initConfig({
        pkg: packageFile,
        authors: authors,

        /**
         * Watch
         *
         * Acompanha mudanças nos arquivos e executa as tasks definidas.
         *
         * @example https://github.com/gruntjs/grunt-contrib-watch
         */
        watch: {
            scripts: {
                files: [
                    'src/js/**/*.js'                    
                ],
                tasks: ['concat','uglify']
            },
            style: {
                files: [
                    'assets/css/**/*.css',
                    '!**/*.min.css'
                ],
                tasks: ['cssmin','copy']
            },
            sass: {
                files: [
                    'src/sass/**/*.scss'
                ],
                tasks: ['sass','copy']
            }
        },

        /**
         * Uglify
         * 
         * Minifica arquivos javascripts
         *
         * @example https://github.com/gruntjs/grunt-contrib-uglify
         */
        uglify: {
            options: { 
                mangle: false, 
                banner:bannerFiles 
            },
            main: {
                files: {
                    'assets/js/main.min.js' : [ 'assets/js/main.js' ]
                }
            }
        },

        /**
         * Contat
         * 
         * Concatena os arquivos javascripts
         *
         * @example https://github.com/gruntjs/grunt-contrib-uglify
         */
        concat: {
            options: {
                separator: '\n',
                stripBanners:true,
                banner:bannerFiles
            },
            main: {
                src: ['src/js/**/*.js'],
                dest: 'assets/js/main.js'
            }
        },

        
        //SASS
        sass: {
          dist: {
            options: {},
            files: {
              'assets/css/style.css' : 'src/sass/style.scss'              
            }
          }
        },


        //CSS MIN
        cssmin: {
            combine: {
                options: {
                    banner:bannerFiles,
                    keepSpecialComments:0
                },
                files: {
                     'assets/css/style.min.css': [
                         'assets/css/reset.css',
                         'assets/css/style.css'
                    ]
                }
            }
        },


        copy: {
		  main: {
		    files: [
		      // includes files within path
		      {expand: true, cwd: 'assets/css/', src: ['**'], dest: '../wp-content/themes/jovianamarques/assets/css/'},
		    ]
		  }
		}





    });

    // Carrega o plugin que disponibiliza a tarefa "uglify"
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // Tarefa(s) padrão.
    grunt.registerTask('default', ['concat','sass','cssmin','uglify']);
    grunt.registerTask('w', [
        'sass',
        'concat',
        'cssmin',
        'uglify',
        'copy',
        'watch'
    ]);
    grunt.registerTask('build'  , ['concat','sass','cssmin','uglify','concat:app','uglify:app','copy']);

};