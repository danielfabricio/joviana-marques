/*
Theme Name: Joviana Marques
Theme URI: jovianamarques.com.br
Description: 2elve desenvolvimento web
Version: 1.0
Author: Daniel Fabricio
License: MIT
License URI: http://opensource.org/licenses/mit-license.php
*/
// DOM Ready
$(function() {

});
	// MENU RESPONSIVO
	$('.menu-responsivo').click(function(){
		$('.small-menu').slideToggle();
	});

	// BUSCA RESPONSIVO
	$('.busca-responsiva').click(function(){
		$('.busca').toggleClass('ativa');
	});